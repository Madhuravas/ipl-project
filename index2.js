const fs = require("fs")
const { get } = require("http")
const path = require("path")

let matchsData = fs.readFileSync("/home/satish/ipl/data/matches.csv", { encoding: "utf-8" })

matchsData = matchsData.split("\n")

const matchHeaders = matchsData[0].split(",")

const matchesResult = []

for (let i = 1; i < matchsData.length - 1; i++) {
    let data = matchsData[i].split(",")
    let obj = {}
    for (var j = 0; j < data.length; j++) {
        obj[matchHeaders[j]] = data[j]
    }
    matchesResult.push(obj)
}


let deliveriesData = fs.readFileSync("/home/satish/ipl/data/deliveries.csv", { encoding: "utf-8" })

deliveriesData = deliveriesData.split("\n")

let deliveriesHeaders = deliveriesData[0].split(",")

let deliveriesResult = []

for (let i = 1; i < deliveriesData.length - 1; i++) {
    let data = deliveriesData[i].split(",")
    let obj = {}
    for (let j = 0; j < data.length; j++) {
        obj[deliveriesHeaders[j]] = data[j]
    }
    deliveriesResult.push(obj)
}



// Q1. Number of matches played per year for all the years in IPL. 

let matchesPlayedPerYearATeam = matchesResult.reduce((acc, eachMatchObj) => {
    acc[eachMatchObj.season] ? acc[eachMatchObj.season] += 1 : acc[eachMatchObj.season] = 1
    return acc
}, {})

const filePath1 = path.join(__dirname, './public/output/matches_playes_per_year.json')

fs.writeFileSync(filePath1, JSON.stringify(matchesPlayedPerYearATeam))

// Q2 Number of matches won per team per year in IPL. 

let numberOfMatchsWinPerTeamPerYear = matchesResult.reduce((acc, eachMatchObj) => {
    if (acc[eachMatchObj.season]) {
        if (acc[eachMatchObj.season][eachMatchObj.winner]) {
            acc[eachMatchObj.season][eachMatchObj.winner] += 1
        } else {
            acc[eachMatchObj.season][eachMatchObj.winner] = 1
        }
    } else {
        let teamsObj = {}
        teamsObj[eachMatchObj.winner] = 1
        acc[eachMatchObj.season] = teamsObj
    }
    return acc
}, {})
let filePath2 = path.join(__dirname, '/public/output/number_of_matchs_win_per_year_Team.json')
fs.writeFileSync(filePath2, JSON.stringify(numberOfMatchsWinPerTeamPerYear))


// Q3 Extra runs conceded per team in the year 2016

let getTheExtraRunsIn2016EachTeam = (matchesResult, deliveriesResult) => {
    let result = {}
    matchesResult.forEach(eachMatch => {
        if (eachMatch.season === "2016") {
            deliveriesResult.forEach((eachItem) => {
                if (eachItem.match_id === eachMatch.id) {
                    if (result[eachItem.bowling_team]) {
                        result[eachItem.bowling_team] += parseInt(eachItem.extra_runs)
                    } else {
                        result[eachItem.bowling_team] = parseInt(eachItem.extra_runs)
                    }
                }
            })
        }
    });
    let filePath = path.join(__dirname, '/public/output/extra_runs.json')
    fs.writeFileSync(filePath, JSON.stringify(result))
}

//getTheExtraRunsIn2016EachTeam(matchesResult,deliveriesResult)


// Q4 Top 10 economical bowlers in the year 2015 

let getTopTenEconomicalBowlersIn2015 = (matchesResult, deliveriesResult) => {
    let allPlayersDetails = {}
    matchesResult.forEach(eachMatch => {
        if (eachMatch.season === "2015") {
            deliveriesResult.forEach((eachItem) => {
                if (eachItem.match_id === eachMatch.id) {
                    if (allPlayersDetails[eachItem.bowler]) {
                        allPlayersDetails[eachItem.bowler].balls += 1
                        allPlayersDetails[eachItem.bowler].runs += parseInt(eachItem.total_runs)
                    } else {
                        let playerBallsAndRuns = {}
                        playerBallsAndRuns.balls = 1
                        playerBallsAndRuns.runs = parseInt(eachItem.total_runs)
                        allPlayersDetails[eachItem.bowler] = playerBallsAndRuns
                    }
                }
            })

        }
    })
    const economyOfPlayers = Object.keys(allPlayersDetails).reduce((acc, playerName) => {
        let data = {}
        let overs = Math.round((allPlayersDetails[playerName].balls) / 6)
        let economy = Math.floor((allPlayersDetails[playerName].runs / overs) * 10) / 10
        data[playerName] = economy
        acc.push(data)
        return acc
    }, [])
    let sortedArray = economyOfPlayers.sort((a, b) => {
        return a[Object.keys(a)] > b[Object.keys(b)] ? 1 : -1
    })

    const filePath = path.join(__dirname, "./public/output/economic_rate.json")
    fs.writeFileSync(filePath, JSON.stringify(economyOfPlayers.slice(0, 10)))
}


getTopTenEconomicalBowlersIn2015(matchesResult, deliveriesResult)

//Q 5 Find the number of times each team won the toss and also won the match. 

let teamWinMatchAndToss = matchesResult.reduce((acc, eachMatch) => {
    if (eachMatch.toss_winner === eachMatch.winner) {
        if (acc[eachMatch.winner]) {
            acc[eachMatch.winner] += 1
        } else {
            acc[eachMatch.winner] = 1
        }
    }
    return acc
}, {})

fs.writeFileSync('public/output/total_wins_match_and_toss.json', JSON.stringify(teamWinMatchAndToss))


//Q6 Find a player who has won the highest number of Player of the Match awards for each season.

let getHighestPlayerOfMatchWinsEachSeason = (matchesResult) => {
    let result = matchesResult.reduce((acc, eachMatch) => {
        if (acc[eachMatch.season]) {
            if (acc[eachMatch.season][eachMatch.player_of_match]) {
                acc[eachMatch.season][eachMatch.player_of_match] += 1
            } else {
                acc[eachMatch.season][eachMatch.player_of_match] = 1
            }
        } else {
            let playerObj = {}
            playerObj[eachMatch.player_of_match] = 1
            acc[eachMatch.season] = playerObj
        }
        return acc
    }, {})

    let resultData = Object.keys(result).map((eachYear) => {
        let data = Object.keys(result[eachYear]).sort((a, b) => {
            return result[eachYear][a] < result[eachYear][b] ? 1 : -1
        })
        let obj = {}
        let result2 = Object.keys(result[eachYear]).reduce((acc, eachPlayer) => {

            if (result[eachYear][data[0]] === result[eachYear][eachPlayer]) {

                obj[eachPlayer] = result[eachYear][eachPlayer]
            }
            acc[eachYear] = obj
            return acc
        }, {})
        return result2
    })
    fs.writeFileSync('public/output/higest_player_of_match_per_year.json', JSON.stringify(resultData))
}

//getHighestPlayerOfMatchWinsEachSeason(matchesResult)


// Question 7 Find the strike rate of a batsman for each season
// strikerate = (totalruns/ballsForced) * 100
let strikeRateOfPlayerByEachYear = (matchsResults, deliveriesResult) => {
    let playerData = deliveriesResult.filter(obj => {
        return obj.batsman === 'V Kohli'
    })

    let playerGamesSeasons = playerData.map((object) => {
        let extractedYear = matchsResults.filter((obj) => {
             return obj.id === object.match_id 
        })
        .map((obj) => { 
            return obj.season 
        })
        object.season = extractedYear[0]
        return object
    })
    
    let result = playerData.reduce((acc,eachDelivery) =>{
       if (acc[eachDelivery.season]){
           acc[eachDelivery.season].balls += 1 
           acc[eachDelivery.season].runs += parseInt(eachDelivery.total_runs)
       }else{
           let obj = {}
           obj.balls = 1 
           obj.runs  = parseInt(eachDelivery.total_runs)
           acc[eachDelivery.season] = obj
       }
       return acc
    },{})
    Object.keys(result).forEach((eachYear) =>{
           let strikeRate = Math.floor((result[eachYear].runs / result[eachYear].balls)*100*10)/10
           result[eachYear].strikeRate = strikeRate
    })
    fs.writeFileSync("public/output/player_each_Season_strikerate.json",JSON.stringify(result))

}

strikeRateOfPlayerByEachYear(matchesResult, deliveriesResult)

//Q8 Find the highest number of times one player has been dismissed by another player 

let getHighestNumberOfTimesDismissedPlayer = (deliveriesResult) => {
    let player = deliveriesResult.reduce((acc, eachDelivery) => {
        if (eachDelivery.player_dismissed != "" && eachDelivery.fielder != "") {
            if (acc[eachDelivery.player_dismissed]) {
                acc[eachDelivery.player_dismissed] += 1
            } else {
                acc[eachDelivery.player_dismissed] = 1
            }

        }
        return acc
    }, {})

    let sortedData = Object.keys(player).sort((a, b) => {
        return player[a] < player[b] ? 1 : -1
    })
    let result = [sortedData[0], player[sortedData[0]]]
    fs.writeFileSync("public/output/highest_times_dismissed_player.json", JSON.stringify(result))

}


//getHighestNumberOfTimesDismissedPlayer(deliveriesResult)


//Q9 Find the bowler with the best economy in super overs 

let getBestEconomyBowlerInSuperOver = (deliveriesResult) => {
    let superOverData = deliveriesResult.reduce((acc, eachDelivery) => {
        if (eachDelivery.is_super_over != "0") {
            if (acc[eachDelivery.bowler]) {
                acc[eachDelivery.bowler].balls += 1
                acc[eachDelivery.bowler].runs += parseInt(eachDelivery.total_runs)
            } else {
                let runsAndBallsData = {}
                runsAndBallsData.balls = 1
                runsAndBallsData.runs = parseInt(eachDelivery.total_runs)
                acc[eachDelivery.bowler] = runsAndBallsData
            }
        }
        return acc
    }, {})
    let playerEconomy = Object.keys(superOverData).reduce((acc, eachPlayer) => {
        let overs = Math.floor(superOverData[eachPlayer].balls / 6 * 10) / 10
        acc[eachPlayer] = Math.floor(superOverData[eachPlayer].runs / overs * 10) / 10
        return acc
    }, {})
    let sortedData = Object.keys(playerEconomy).sort((a, b) => {
        return playerEconomy[a] > playerEconomy[b] ? 1 : -1
    })
    fs.writeFileSync('public/output/best_economy_in_superover.json', JSON.stringify(sortedData[0]))
}


//getBestEconomyBowlerInSuperOver(deliveriesResult)


