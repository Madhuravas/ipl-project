//chart 1

const fetchData = (generateChart) => {
    fetch("./output/matches_playes_per_year.json")
        .then(data => data.json())
        .then(data => {
            generateChart(data)
        })
}


let generateChart = (data) => {
    let transForData = Object.entries(data)
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Each Team played matches Peryear'
        },
        subtitle: {
            text: 'Problem 1'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total Number of games'
            }
        },
        
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Population in 2021: <b>{point.y} milli</b>'
        },
        series: [{
            name: 'Population',
            data: transForData,
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    })
}
fetchData(generateChart)

// chart 3
const fetchData2 = (generateChart2) => {
    fetch("./output/extra_runs.json")
        .then(data => data.json())
        .then(data => {
            generateChart2(data)
        })
}


let generateChart2 = (data) =>{
    let changeData = Object.entries(data)
    

Highcharts.chart('container1', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Total number of runs get by each team'
    },
    subtitle: {
        text: 'Problem 3'
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Number of runs'
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Population in 2021: <b>{point.y:.1f} millions</b>'
    },
    series: [{
        name: 'Population',
        data: changeData,
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF', 
            align: 'right',
            format: '{point.y}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
})
};
fetchData2(generateChart2)
